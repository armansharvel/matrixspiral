//
//  main.c
//  MatrixSpiral
//
//  Created by Arman Arutyunov on 19/05/2017.
//  Copyright © 2017 Arman Arutyunov. All rights reserved.
//

#include <stdio.h>
#include <stdbool.h>

void printIntro();
int getSize();
bool sizeIsCorrect();

void enterTheMatrix();
void buildAndPrintMatrix();
void printSpiral();

int n, matrixSize;



int main(int argc, const char * argv[]) {
	
	printIntro();
	enterTheMatrix();
	
	return 0;
}

void printIntro() {
	
	printf("Input the \"N\" multiplier for \"2n-1\" formula to get the matrix size: ");
	scanf("%d", &n);
	printf("\n");
	
	matrixSize = getSize(n);
	if (sizeIsCorrect(matrixSize)) {
		printf("Matrix size = %dx%d\n\n", matrixSize, matrixSize);
	} else {
		printIntro();
	}
}

int getSize(int n) {
	return 2*n-1;
}

bool sizeIsCorrect(int size) {
	
	if (size <= 1) {
		printf("Sorry, the matrix size is too small\nTry again\n\n");
		return false;
	}
	return true;
}

void enterTheMatrix() {
	
	int a[matrixSize][matrixSize];
	buildAndPrintMatrix(matrixSize, a, 1);
	
	printf("\nLined up spiral result:\n\n");
	printSpiral(matrixSize, a);
	printf("\n\n");
}

void buildAndPrintMatrix(int matrixSize, int a[matrixSize][matrixSize], int value){
	
	for (int i = 0; i < matrixSize; i++) {
		for (int j = 0; j < matrixSize; j++) {
			a[i][j] = value++;
			printf("%2d ", a[i][j]);
		}
		printf("\n");
	}
}

void printSpiral(int matrixSize, int a[matrixSize][matrixSize]) {
	
	int round, radius, row, column;
	
	radius = row = column = (matrixSize - 1) / 2;
	
	for (round = 1; round <= matrixSize - radius; round++) {
		
		for (; column >= radius - round; column--) { //LEFT
			printf("%4d", a[row][column]);
			if (row < 1 && column < 1)
				return;
		}
		
		for (row++, column++; row <= radius + round; row++)  //DOWN
			printf("%4d", a[row][column]);
		
		for (row--, column++; column <= radius + round; column++)  //RIGHT
			printf("%4d", a[row][column]);
		
		for (row--, column--; row >= radius - round; row--)  //UP
			printf("%4d", a[row][column]);
		
		row++;
		column--;
	}
}








